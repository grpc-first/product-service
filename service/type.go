package service

import (
	"context"

	pb "gitlab.com/product-service/genproto/product"
	"gitlab.com/product-service/pkg/logger"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (p *ProductService) CreateType(ctx context.Context, req *pb.Type) (*pb.Type, error) {
	res, err := p.Storage.Product().CreateType(req)
	if err != nil {
		p.Logger.Error("Error wile creating Type", logger.Any("Create", err))
		return &pb.Type{}, status.Error(codes.Internal, "Please recheck type info")
	}
	return res, nil
}

func (p *ProductService) GetTypeById(ctx context.Context, req *pb.GetById) (*pb.Type, error) {
	res, err := p.Storage.Product().GetTypeById(req.Id)
	if err != nil {
		p.Logger.Error("Error while getting type info", logger.Any("Get", err))
		return &pb.Type{}, status.Error(codes.NotFound, "Does not Exist")
	}
	return res, nil
}
