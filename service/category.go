package service

import (
	"context"

	pb "gitlab.com/product-service/genproto/product"
	"gitlab.com/product-service/pkg/logger"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (p *ProductService) CreateCategory(ctx context.Context, req *pb.Category) (*pb.Category, error) {
	res, err := p.Storage.Product().CreateCategory(req)
	if err != nil {
		p.Logger.Error("Error while creating category", logger.Any("insert", err))
		return &pb.Category{}, status.Error(codes.InvalidArgument, "Please recheck user info")
	}
	return res, nil
}

func (p *ProductService) GetCategoryById(ctx context.Context, req *pb.GetById) (*pb.Category, error) {
	res, err := p.Storage.Product().GetCategoryById(req.Id)
	if err != nil {
		p.Logger.Error("Error wile getting category by id", logger.Any("get", err))
		return &pb.Category{}, status.Error(codes.NotFound, "Target Category not found")
	}
	return res, nil
}
