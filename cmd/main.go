package main

import (
	"net"

	"gitlab.com/product-service/config"
	pb "gitlab.com/product-service/genproto/product"
	"gitlab.com/product-service/pkg/db"
	"gitlab.com/product-service/pkg/logger"
	"gitlab.com/product-service/service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()

	log := logger.New(cfg.LogLevel, "product-service")
	defer logger.CleanUp(log)

	log.Info("main: sqlx Config",
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase),
	)

	connDb, err := db.ConnectToDb(cfg)
	if err != nil {
		log.Fatal("Error while connecting to database: %v", logger.Error(err))
	}

	productService := service.NewProductService(connDb, log)

	lis, err := net.Listen("tcp", cfg.GRPCPort)
	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

	s := grpc.NewServer()
	reflection.Register(s)
	pb.RegisterProductServiceServer(s, productService)
	log.Info("Server is running", logger.String("Port", cfg.GRPCPort))

	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
}
