package repo

import pb "gitlab.com/product-service/genproto/product"

type ProductStorageI interface {
	CreateProduct(*pb.Product) (*pb.ProductInfo, error)
	GetAllProducts() (*pb.GetProducts, error)
	GetProductsByIds(*pb.GetProductsByIdsRequest) (*pb.GetProducts, error)

	CreateType(req *pb.Type) (*pb.Type, error)
	GetTypeById(id int64) (*pb.Type, error)

	CreateCategory(req *pb.Category) (*pb.Category, error)
	GetCategoryById(id int64) (*pb.Category, error)
}
