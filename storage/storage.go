package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/product-service/storage/postgres"
	"gitlab.com/product-service/storage/repo"
)

type IsStorage interface {
	Product() repo.ProductStorageI
}

type StoragePg struct {
	Db       *sqlx.DB
	ProductRepo repo.ProductStorageI
}

func NewStoragePg(db *sqlx.DB) *StoragePg {
	return &StoragePg{
		Db:       db,
		ProductRepo: postgres.NewProductRepo(db),
	}
}

func (s StoragePg) Product() repo.ProductStorageI {
	return s.ProductRepo
}
